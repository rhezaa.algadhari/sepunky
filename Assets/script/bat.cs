﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class bat : Enemy 
{
    [SerializeField] Vector3 init_pos;
    [SerializeField] float distance, distanceY, radiusChase;
    NavMeshAgent nav;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        _state = "idle";
        init_pos = transform.position;
        nav = GetComponent<NavMeshAgent>();
        //player = FindObjectOfType<PlayerChar>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        distance = Mathf.Abs(player.transform.position.x - transform.position.x);
        distanceY = Mathf.Abs((player.transform.position.y -3) - transform.position.y);
             
             

        if(_state == "idle")
        {
            anim.SetInteger("state", 0);
        }
        else if(_state == "chase")
        {
            anim.SetInteger("state", 1);
        }

        if(distance <= radiusChase && distanceY <= 6)
        {

            StartCoroutine("chase");
        }
        else
        {
            StartCoroutine("idle");
        }

        

    }

    IEnumerator chase()
    {
        _state = "chase";
        nav.SetDestination(player.transform.position);
        yield return new WaitForSeconds(0.1f);
    }

    IEnumerator idle()
    {
        _state = "idle";
        nav.SetDestination(init_pos);
        yield return new WaitForSeconds(0.1f);
    }
}
