﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerChar : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;
    [SerializeField] float speed ;
    bool faceright;
    [SerializeField] float jumpforce;
    [SerializeField]GameObject handpos;
    [SerializeField]bool can_interact, bring_item, grounded, can_climb, has_key, invincible;
    [SerializeField] GameObject itemObject, carrytext;
    public UnityEvent pickup, lempar, lompat, hit;
    CapsuleCollider capsule_col;
    public int lives;
    [SerializeField] GameObject model;
    [SerializeField] Material[] mat;
    Material damaged;
    Vector3 movement;
    [SerializeField]Vector3 init_pos;
    [SerializeField] Text textlives;
    

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0, 0, 0);
        faceright = true;
        can_interact = false;
        bring_item = false;
        capsule_col = GetComponent<CapsuleCollider>();
        init_pos = transform.position;
        anim.SetInteger("state", 0);
        
    }

    // Update is called once per frame
    void Update()
    {
        //textlives.setTE

        if (Input.GetKeyDown(KeyCode.R))
        {
            respawn();
        }

        if(lives < 0)
        {
            respawn();

        }

        float move = Input.GetAxis("Horizontal");
        float move_up = Input.GetAxis("Vertical");
        
        movement = new Vector3(speed * move, rb.velocity.y, speed *move_up);

        rb.velocity = movement;

        if (Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0 && grounded)
        {
            if (!bring_item)
            {
                anim.SetInteger("state", 0);
            }
            else
            {
                anim.SetInteger("state", 6);
            }
            
        }

        if ((Input.GetAxis("Horizontal") !=0 || Input.GetAxis("Vertical") != 0) && grounded)
        {
            
            if (!bring_item)
            {
                anim.SetInteger("state", 3);
            }
            else
            {
                anim.SetInteger("state", 4);
            }
        }



        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine("jump");
            
            
        }

        if (can_climb)
        {
            //Quaternion climbrot = new Quaternion(0, 0, 0, 0 );

            //transform.rotation = climbrot;

            if (Input.GetAxis("Vertical") != 0)
            {
                //capsule_col.isTrigger = true;
                rb.velocity = new Vector3(rb.velocity.x, speed * move_up, 0);
                rb.useGravity = false;

                if (!bring_item)
                {
                    anim.SetInteger("state", 2);
                }
                else
                {
                    anim.SetInteger("state", 4);
                }
                
            }
            else if (Input.GetAxis("Vertical") == 0)
            {
                rb.velocity = new Vector3(rb.velocity.x, 0, 0);
                anim.SetInteger("state", 0);
                //defaultrotation();
            }
                      
            
        }
        
       
        

        if (move > 0 && !faceright) Flip();
        else if (move < 0 && faceright) Flip();

        if (bring_item)
        {
            itemObject.transform.position = handpos.transform.position;
            carrytext.SetActive(false);
        }

        
        

        if (Input.GetKeyDown(KeyCode.F)){
            if(!bring_item && can_interact)
            {
                if (itemObject.CompareTag("key"))
                {
                    has_key = true;
                }

                bring_item = true;                
                can_interact = false;
                pickup.Invoke();
            }
            else if (bring_item)
            {
                if (itemObject.CompareTag("key"))
                {
                    has_key = false;
                    itemObject = null;
                }

                else
                {
                    lempar.Invoke();
                    bring_item = false;
                    Rigidbody itemrb = itemObject.GetComponent<Rigidbody>();
                    itemrb.velocity = new Vector3(0, 0, 0);
                    if (faceright)
                    {
                        itemrb.AddForce(new Vector3(12000, 1000, 0));
                    }
                    else
                    {
                        itemrb.AddForce(new Vector3(-12000, 1000, 0));
                    }
                    can_interact = false;
                    itemObject = null;
                }

                
               
                
            }

        }
    }

    void Flip()
    {
        if (faceright)
        {
            Quaternion flip = new Quaternion(0, 180, 0, 0);
            transform.Rotate(0,180,0);
            //transform.rotation = transform.rotation = Quaternion.RotateTowards(transform.rotation, flip, 5 * Time.deltaTime);
            faceright = !faceright; 
        }
        else if (!faceright)
        {
            Quaternion flip = new Quaternion(0, 180, 0, 0);
            transform.Rotate(0,180,0);
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, flip, 5*Time.deltaTime);
            faceright = !faceright;
        }

        /*
        faceright = !faceright;
        Vector3 theScale = transform.localScale;
        theScale.z *= -1;
        transform.localScale = theScale;
        */
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("object"))
        {
            grounded = true;

            if (!bring_item)
            {
                can_interact = true;
                itemObject = collision.gameObject;
                carrytext.SetActive(true);
               
            }               
            
            
        }

        if (collision.gameObject.CompareTag("key"))
        {
            grounded = true;

            if (!bring_item)
            {
                can_interact = true;
                itemObject = collision.gameObject;
                carrytext.SetActive(true);
            }
        }

        if (collision.gameObject.CompareTag("ground"))
        {
            grounded = true;
        }

        if(collision.gameObject.CompareTag("obstacle") || collision.gameObject.CompareTag("enemy"))
        {
            if (!invincible)
            {
                hit.Invoke();
                StartCoroutine("takingDamage");
            }
            

        }

        if (collision.gameObject.CompareTag("ladder"))
        {
            can_climb = true;

        }

        if (collision.gameObject.CompareTag("door"))
        {
            if (has_key)
            {
                menang();
            }
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("object"))
        {
            if (!bring_item)
            {
                can_interact = false;
                itemObject = null;
                carrytext.SetActive(false);
            }

        }

        if (collision.gameObject.CompareTag("key"))
        {
            if (!bring_item)
            {
                can_interact = false;
                itemObject = null;
                carrytext.SetActive(false);
            }

        }

        if (collision.gameObject.CompareTag("ground"))
        {
            grounded = false;
        }

        if (collision.gameObject.CompareTag("ladder"))
        {
            can_climb = false;
            //capsule_col.isTrigger = false;
            rb.useGravity = true;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        

        
    }

    private void OnTriggerExit(Collider other)
    {
       

        
    }

    IEnumerator takingDamage()
    {
        invincible = true;
        lives -= 1;
        
        if (faceright)
        {
            
            rb.AddForce(new Vector3(-1000f, 1000f, 0));
        }
        else if (!faceright)
        {
            rb.AddForce(new Vector3(1000f, 1000f, 0));

        }
       
        
        yield return new WaitForSeconds(1);
        invincible = false;


    }

    void respawn()
    {
        rb.velocity = new Vector3(0, 0, 0);
        transform.position = init_pos;
        lives = 3;
    }
    
    IEnumerator jump()
    {
       

        if (grounded)
        {
            lompat.Invoke();
            rb.AddForce(new Vector3(rb.velocity.x, jumpforce, 0));
            

        }
        yield return new WaitForSeconds(0.1f);
        if (!grounded)
        {
            if (!bring_item)
            {
                anim.SetInteger("state", 1);
            }
            else
            {
                anim.SetInteger("state", 5);
            }
            
        }

    }

    


    void menang()
    {
        SceneManager.LoadScene("menang");
    }
}
