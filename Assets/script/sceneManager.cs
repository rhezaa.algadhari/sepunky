﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManager : MonoBehaviour
{
    [SerializeField] string scenetujuan;
    [SerializeField] KeyCode tombol;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(tombol))
        {
            lompatScene(scenetujuan);
        }
    }

    public void lompatScene(string tujuan)
    {
        SceneManager.LoadScene(tujuan);
    }
}
