﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class snake : Enemy
{
    [SerializeField]Transform[] patrol_pos;
    NavMeshAgent nav;
    public int i;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        
        
        nav = GetComponent<NavMeshAgent>();

        StartCoroutine("Patrol");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position == patrol_pos[i].position)
        {
            

                 i += 1; 

                if(i == 4 )
                {
                    i = 0;
                }
            
        }
    }

    IEnumerator Patrol()
    {
        nav.SetDestination(patrol_pos[i].position);
            

        yield return new WaitForSeconds(0.5f);

        StartCoroutine("Patrol");
    }
}
